
variable "engine" {}
variable "instance_class" {}
variable "db_name" {}
variable "username" {}
variable "password" {}
variable "allocated_storage" {}
variable "skip_final_snapshot" {
  type = bool
}

# variable "vpc_security_group_ids" {
#   type = list(string)
# }


#subnetdb

variable "name" {}
variable "subnet_ids" {
  type = list(string)
}

variable "vpc_id_for_rds_sg" {

}

#security group
variable "allowed_sg" {
  type = list(string)

}
