
resource "aws_db_instance" "my_rds_instance" {
  engine                 = var.engine
  instance_class         = var.instance_class
  db_name                = var.db_name
  username               = var.username
  password               = var.password
  allocated_storage      = var.allocated_storage
  skip_final_snapshot    = var.skip_final_snapshot
  db_subnet_group_name   = aws_db_subnet_group.my_db_subnet_group.name
  vpc_security_group_ids = [aws_security_group.rds-sg.id]
}

resource "aws_db_subnet_group" "my_db_subnet_group" {
  name       = var.name
  subnet_ids = var.subnet_ids
}

resource "aws_security_group" "rds-sg" {
  name_prefix = "rds-group-from-tf"
  description = "This sg is genereated by tf"
  vpc_id      = var.vpc_id_for_rds_sg

  # Inbound rules (ingress rules)
  ingress {
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = var.allowed_sg
  }
}




