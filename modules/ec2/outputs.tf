# This file defines the outputs for the EC2 instance module

output "instance_id" {
  value = aws_instance.web_server.id
}

output "public_ip" {

  value = aws_instance.web_server.public_ip
}

output "ec2-sg-id" {
  value = aws_security_group.example_security_group.id
}

output "ec2-private-id" {
  value = aws_instance.web_server.private_ip
}

# Additional outputs can be defined here to expose more information about the EC2 instance.
