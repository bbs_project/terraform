# This file defines input variables for the EC2 instance module

variable "ami_id" {
  description = "AMI ID for the EC2 instance"
}

variable "instance_name" {
  description = "AMI name for the EC2 instance"
}

variable "instance_type" {
  description = "Instance type for the EC2 instance"
}

variable "subnet_id_for_ec2" {
  description = "Subnet ID where the EC2 instance will be launched"
}

variable "vpc_id_for_sg" {
  description = "vpc_id"
}

variable "key_name" {
  description = "key_name"
}

variable "enable_public_ip" {
  description = "enable_public_ip"
  type        = bool
}

variable "ec2_role" {
  description = "role"
}
# Additional variables can be added here as needed.
