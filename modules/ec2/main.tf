resource "aws_security_group" "example_security_group" {
  name_prefix = "groupfromtf"
  description = "This sg is genereated by tf"
  vpc_id      = var.vpc_id_for_sg

  # Inbound rules (ingress rules)
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 9000
    to_port     = 9000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "web_server" {
  ami                         = var.ami_id
  instance_type               = var.instance_type
  subnet_id                   = var.subnet_id_for_ec2
  key_name                    = var.key_name
  associate_public_ip_address = var.enable_public_ip
  tags = {
    Name = var.instance_name
  }
  iam_instance_profile   = var.ec2_role
  vpc_security_group_ids = [aws_security_group.example_security_group.id]
}

resource "aws_eip" "example_eip" {
  instance = aws_instance.web_server.id
}
