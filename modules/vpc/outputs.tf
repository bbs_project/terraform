# This file defines the outputs for the VPC module

output "vpc_id" {
  value = aws_vpc.main.id
}

output "private_subnet_ids_1" {
  value = aws_subnet.private_subnet[1].id
}

output "private_subnet_ids_2" {
  value = aws_subnet.private_subnet[2].id
}

output "public_subnet_ids" {
  value = aws_subnet.public_subnet[1].id
}
