# This file defines input variables for the VPC module

variable "name" {
  description = "Name of the VPC"
}

variable "cidr" {
  description = "CIDR block for the VPC"
}

variable "azs" {
  description = "Availability zones for the VPC subnets"
  type        = list(string)
}

variable "private_subnets" {
  description = "CIDR blocks for the private subnets"
  type        = list(string)
}

variable "public_subnets" {
  description = "CIDR blocks for the public subnets"
  type        = list(string)
}

variable "public_subnets_name" {
  description = "Name for the public subnets"
  type        = list(string)
}

variable "private_subnets_name" {
  description = "Name for the public subnets"
  type        = list(string)
}
