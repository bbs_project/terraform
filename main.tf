module "vpc-with-subnet" {
  source = "./modules/vpc"
  name   = "my-vpc-from-tf"
  cidr   = "10.29.0.0/16"

  azs = ["ap-northeast-3a", "ap-northeast-3b", "ap-northeast-3c"]
  # azs = ["ap-east-1a", "ap-east-1b", "ap-east-1c"]

  public_subnets_name = ["public1", "public2", "public3"]
  public_subnets      = ["10.29.10.0/24", "10.29.11.0/24", "10.29.12.0/24"]

  private_subnets_name = ["private1", "private2", "private3"]
  private_subnets      = ["10.29.20.0/24", "10.29.21.0/24", "10.29.22.0/24"]

}

# Create 1 key pair for EC2
module "keypair" {
  source = "./modules/keypair"
}

module "ec2-with-sg" {
  source = "./modules/ec2"
  #HongKong image
  # ami_id = "ami-09dcd6cddb9c10766"

  #OSAKA image
  ami_id        = "ami-07d1895b68d6e10ea"
  instance_name = "EC2 from TF"
  #OSAKA instace_type
  instance_type     = "t3.medium"
  key_name          = module.keypair.ec2-key
  enable_public_ip  = true
  subnet_id_for_ec2 = module.vpc-with-subnet.public_subnet_ids
  vpc_id_for_sg     = module.vpc-with-subnet.vpc_id
  ec2_role          = "ecsInstanceRole"
}

#Create 1 ECS
module "ecs" {
  source = "./modules/ecs"
}

#Create 2 private ECR repos
module "ecr" {
  source = "./modules/ecr"
}

# subnet, rds
module "rds" {
  source              = "./modules/rds"
  engine              = "mysql"
  instance_class      = "db.t3.micro"
  db_name             = "mydbfromtf"
  username            = "root"
  password            = "nghia270901"
  allocated_storage   = 5
  skip_final_snapshot = true
  name                = "my-db-subnet-group-fromtf"
  subnet_ids          = [module.vpc-with-subnet.private_subnet_ids_1, module.vpc-with-subnet.private_subnet_ids_2]
  vpc_id_for_rds_sg   = module.vpc-with-subnet.vpc_id
  allowed_sg          = [module.ec2-with-sg.ec2-sg-id]
}

























